const minimum = document.getElementById('minimum');
const maximum = document.getElementById('maximum');
const button = document.getElementById('get-result');
const resultField = document.getElementById('result-field');
const decimal = document.getElementById('decimal');
const wrong = document.getElementById('wrong');
const wrongNum = document.getElementById('wrong-num');

button.addEventListener('click', getRandom);

function getRandom(){
  let random = Math.random() * (parseInt(maximum.value) - parseInt(minimum.value) + 1) + parseInt(minimum.value);
  if (isNaN(minimum.value) || isNaN(maximum.value)){
    wrongNum.classList.remove('hidden');
  } else {
      if (parseInt(minimum.value) >= parseInt(maximum.value)) {
       wrong.classList.add('visible');
       wrongNum.classList.add('hidden');
    }else if (decimal.checked){
       resultField.innerHTML = Math.floor(random);
      wrong.classList.remove('visible');
      wrongNum.classList.add('hidden');
     } else {
       resultField.innerHTML = random.toFixed(2);
       wrong.classList.remove('visible');
       wrongNum.classList.add('hidden');
     }
  }
}
